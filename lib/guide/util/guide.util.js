"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isGuideWasNotSeen = isGuideWasNotSeen;
exports.getAllSpotlightsFromUnseenGuides = getAllSpotlightsFromUnseenGuides;
exports.getInitValue = getInitValue;
exports.mapGuideSchemesToReactComponents = mapGuideSchemesToReactComponents;
exports.getDataFromSpotlightReactComponent = getDataFromSpotlightReactComponent;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _react = _interopRequireDefault(require("react"));

var _shortid = _interopRequireDefault(require("shortid"));

var _guideSpotlight = _interopRequireDefault(require("../components/guideSpotlight.component"));

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default.signature : function (a) {
  return a;
};

var ACTIONS = {
  close: {
    onClick: 'finish',
    text: 'Close'
  },
  next: {
    onClick: 'next',
    text: 'Next'
  },
  back: {
    onClick: 'back',
    text: 'Back'
  },
  skip: {
    onClick: 'finish',
    text: 'Skip'
  },
  remindLater: {
    onClick: 'remindLater',
    text: 'Remind later'
  },
  dismiss: {
    onClick: 'finish',
    text: 'Dismiss'
  },
  done: {
    onClick: 'finish',
    text: 'Done'
  }
};

function deleteLastSpotlight(spotlights) {
  return spotlights.slice(0, spotlights.length - 1);
}

function filterFinishMethods(actions) {
  return actions.filter(function (_ref) {
    var onClick = _ref.onClick;
    return !['finish', 'close', 'remindLater'].some(function (e) {
      return e === onClick;
    });
  });
}

function isNextMethodExist(actions) {
  return actions.some(function (_ref2) {
    var onClick = _ref2.onClick;
    return onClick === 'next';
  });
}

function isBackMethodExist(actions) {
  return actions.some(function (_ref3) {
    var onClick = _ref3.onClick;
    return onClick === 'back';
  });
}

function addNextMethod(actions) {
  var nextAction = ACTIONS.next;

  if (isNextMethodExist(actions)) {
    return actions;
  }

  return [].concat((0, _toConsumableArray2.default)(actions), [nextAction]);
}

function addBackMethod(actions) {
  var backAction = ACTIONS.back;

  if (isBackMethodExist(actions)) {
    return actions;
  }

  return [backAction].concat((0, _toConsumableArray2.default)(actions));
}

function convertLastSpotlightFinishToNextMethod(spotlights) {
  var firstSpotlight = spotlights[0];
  var isOneSpotlight = spotlights.length === 1;

  if (isOneSpotlight) {
    return _objectSpread({}, firstSpotlight, {
      props: _objectSpread({}, firstSpotlight.props, {
        actions: (0, _toConsumableArray2.default)(addNextMethod(firstSpotlight.props.actions))
      })
    });
  }

  var lastSpotlight = spotlights[spotlights.length - 1];
  return _objectSpread({}, lastSpotlight, {
    props: _objectSpread({}, lastSpotlight.props, {
      actions: (0, _toConsumableArray2.default)(addNextMethod(filterFinishMethods(lastSpotlight.props.actions)))
    })
  });
}

function convertFirstSpotlightFinishToBackMethod(spotlights) {
  var spotlight = spotlights[0];
  var isOneSpotlight = spotlights.length === 1;

  if (isOneSpotlight) {
    return _objectSpread({}, spotlight, {
      props: _objectSpread({}, spotlight.props, {
        actions: (0, _toConsumableArray2.default)(addBackMethod(spotlight.props.actions))
      })
    });
  }

  return _objectSpread({}, spotlight, {
    props: _objectSpread({}, spotlight.props, {
      actions: (0, _toConsumableArray2.default)(addBackMethod(filterFinishMethods(spotlight.props.actions)))
    })
  });
}

function deleteFirstSpotlight(spotlights) {
  return spotlights.slice(1, spotlights.length);
}

function isGuideWasNotSeen(seenGuidesVersions, guide) {
  return !seenGuidesVersions.some(function (v) {
    return v === guide.version;
  });
}

function prepareFirstGuide(spotlights) {
  return [].concat((0, _toConsumableArray2.default)(deleteLastSpotlight(spotlights)), [convertLastSpotlightFinishToNextMethod(spotlights)]);
}

function prepareLastGuide(spotlights) {
  return [convertFirstSpotlightFinishToBackMethod(spotlights)].concat((0, _toConsumableArray2.default)(deleteFirstSpotlight(spotlights)));
}

function prepareMiddleGuide(spotlights) {
  var spotlightsWithoutFirstAndLastElements = spotlights.slice(1, spotlights.length - 1);

  if (spotlights.length === 1) {
    var firstSpotlightWithBackAndNextMethod = spotlights.map(function (spot) {
      return _objectSpread({}, spot, {
        props: _objectSpread({}, spot.props, {
          actions: (0, _toConsumableArray2.default)(filterFinishMethods((0, _toConsumableArray2.default)(addBackMethod((0, _toConsumableArray2.default)(addNextMethod(spot.props.actions))))))
        })
      });
    });
    return [firstSpotlightWithBackAndNextMethod[0]];
  }

  return [convertFirstSpotlightFinishToBackMethod(spotlights)].concat((0, _toConsumableArray2.default)(spotlightsWithoutFirstAndLastElements), [convertLastSpotlightFinishToNextMethod(spotlights)]);
}

function getAllSpotlightsFromUnseenGuides(seenGuidesVersions, guides) {
  var mergedSpotlights = [];
  var versions = [];
  var unseendGuides = guides.filter(function (guide) {
    return isGuideWasNotSeen(seenGuidesVersions, guide);
  });

  if (unseendGuides.length === 1) {
    return {
      spotlights: unseendGuides[0].spotlights,
      versions: [unseendGuides[0].version]
    };
  }

  unseendGuides.forEach(function (guide, index) {
    var spotlights = guide.spotlights;
    var version = guide.version;
    versions.push(version);
    var isFirstGuide = index === 0;
    var isLastGuide = index === unseendGuides.length - 1;

    if (isFirstGuide) {
      spotlights = prepareFirstGuide(spotlights);
    } else if (isLastGuide) {
      spotlights = prepareLastGuide(spotlights);
    } else {
      spotlights = prepareMiddleGuide(spotlights);
    }

    mergedSpotlights.push.apply(mergedSpotlights, (0, _toConsumableArray2.default)(spotlights));
  });
  return {
    spotlights: mergedSpotlights,
    versions: versions
  };
}

function getInitValue(spotlights) {
  return spotlights.length ? 0 : null;
}

function mapActionsNameToObjects(actions) {
  return actions.map(function (action) {
    return ACTIONS[action];
  });
}

function generateSpotlightKey() {
  return "spotlight-".concat(_shortid.default.generate());
}

function mapSpotlightSchemeJsonToReactComponents(spotlightSchemes) {
  return spotlightSchemes.map(function (scheme) {
    var mappedScheme = _objectSpread({}, scheme, {
      actions: mapActionsNameToObjects(scheme.actions)
    });

    return /*#__PURE__*/_react.default.createElement(_guideSpotlight.default, (0, _extends2.default)({
      key: generateSpotlightKey()
    }, mappedScheme));
  });
}

function mapGuideSchemesToReactComponents(guideSchemes) {
  return guideSchemes.map(function (guidScheme) {
    var version = guidScheme.version,
        spotlightsSchemes = guidScheme.spotlightsSchemes;
    return {
      version: version,
      spotlights: mapSpotlightSchemeJsonToReactComponents(spotlightsSchemes)
    };
  });
}

function getDataFromSpotlightReactComponent(spotlight) {
  var _spotlight$props = spotlight.props,
      header = _spotlight$props.header,
      position = _spotlight$props.position,
      content = _spotlight$props.content,
      target = _spotlight$props.target,
      width = _spotlight$props.width;
  return {
    header: header,
    position: position,
    content: content,
    target: target,
    width: width
  };
}

;

(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ACTIONS, "ACTIONS", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(deleteLastSpotlight, "deleteLastSpotlight", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(filterFinishMethods, "filterFinishMethods", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isNextMethodExist, "isNextMethodExist", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isBackMethodExist, "isBackMethodExist", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(addNextMethod, "addNextMethod", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(addBackMethod, "addBackMethod", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertLastSpotlightFinishToNextMethod, "convertLastSpotlightFinishToNextMethod", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(convertFirstSpotlightFinishToBackMethod, "convertFirstSpotlightFinishToBackMethod", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(deleteFirstSpotlight, "deleteFirstSpotlight", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(isGuideWasNotSeen, "isGuideWasNotSeen", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(prepareFirstGuide, "prepareFirstGuide", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(prepareLastGuide, "prepareLastGuide", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(prepareMiddleGuide, "prepareMiddleGuide", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getAllSpotlightsFromUnseenGuides, "getAllSpotlightsFromUnseenGuides", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getInitValue, "getInitValue", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapActionsNameToObjects, "mapActionsNameToObjects", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(generateSpotlightKey, "generateSpotlightKey", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapSpotlightSchemeJsonToReactComponents, "mapSpotlightSchemeJsonToReactComponents", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(mapGuideSchemesToReactComponents, "mapGuideSchemesToReactComponents", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
  reactHotLoader.register(getDataFromSpotlightReactComponent, "getDataFromSpotlightReactComponent", "D:\\projects\\apdc-react-guide\\src\\main\\js\\guide\\util\\guide.util.jsx");
})();

;

(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();