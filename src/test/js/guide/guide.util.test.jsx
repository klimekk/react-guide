import React from 'react';
import { isEqual, isEqualWith } from 'lodash';
import expect from 'expect';
import {
  getAllSpotlightsFromUnseenGuides,
  getDataFromSpotlightReactComponent,
  getInitValue,
  isGuideWasNotSeen,
  mapGuideSchemesToReactComponents,
} from '../../../main/js/guide/util/guide.util';
import GuideSpotlight from '../../../main/js/guide/components/guideSpotlight.component';

function sortSpotlights(spotlights) {
  return spotlights.map(spotlight => ({
    ...spotlight,
    props: {
      ...spotlight.props,
      actions: spotlight.props.actions.sort((a, b) => {
        if (a.onClick > b.onClick) return -1;
        if (b.onClick > a.onClick) return 1;
        return 0;
      }),
    },
  }));
}

describe('Guide util', () => {
  it('should return true when guide was not seen by user', () => {
    const seenGuidesVersions = ['TMP-1', 'TMP-2'];
    const guide = {
      version: 'TMP-3',
    };

    const result = isGuideWasNotSeen(seenGuidesVersions, guide);

    expect(result).toBe(true);
  });

  it('should return false when guide was seen', () => {
    const seenGuidesVersions = ['TMP-1', 'TMP-2'];
    const guide = {
      version: 'TMP-1',
    };

    const result = isGuideWasNotSeen(seenGuidesVersions, guide);

    expect(result).toBe(false);
  });

  it('should return 0 when spotlights exist', () => {
    const spotlights = [
      <GuideSpotlight target="any" content="any" key="key" />,
    ];

    const result = getInitValue(spotlights);

    expect(result).toBe(0);
  });

  it('should return null when spotlights no exist', () => {
    const spotlights = [];

    const result = getInitValue(spotlights);

    expect(result).toBe(null);
  });

  it('should return list of spotlights react components', () => {
    const guidesSchemes = [
      {
        version: 'first-guide',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
    ];

    const {
      target,
      actions,
      header,
      position,
      content,
    } = guidesSchemes[0].spotlightsSchemes[0];

    const expected = [
      {
        spotlights: [
          <GuideSpotlight
            key="spotlight-1"
            target={target}
            content={content}
            actions={actions}
            header={header}
            position={position}
          />,
        ],
        version: 'first-guide',
      },
    ];

    const result = mapGuideSchemesToReactComponents(guidesSchemes);

    expect(expected).toEqual(result);
  });

  it('should return guide without spotlights when guides were seen', () => {
    const seenGuidesVersions = ['TMP-1', 'TMP-2'];
    const guides = [
      {
        version: 'TMP-1',
        spotlights: [],
      },
      {
        version: 'TMP-2',
        spotlights: [],
      },
    ];

    const expected = {
      versions: [],
      spotlights: [],
    };

    const result = getAllSpotlightsFromUnseenGuides(seenGuidesVersions, guides);

    expect(result).toEqual(expected);
  });

  it('should join two guides to one when none were seen', () => {
    const seenGuidesVersions = [];
    const guides = [
      {
        version: 'TMP-1',
        spotlights: [
          <GuideSpotlight
            target="any-target"
            content="any-content"
            key="spotlight-1"
          />,
        ],
      },
      {
        version: 'TMP-2',
        spotlights: [
          <GuideSpotlight
            target="any-target"
            content="any-content"
            key="spotlight-1"
          />,
        ],
      },
    ];

    const expected = {
      versions: ['TMP-1', 'TMP-2'],
      spotlights: [
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[{ onClick: 'next', text: 'Next' }]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[{ onClick: 'back', text: 'Back' }]}
        />,
      ],
    };
    const result = getAllSpotlightsFromUnseenGuides(seenGuidesVersions, guides);

    expect(result).toEqual(expected);
  });

  it('should convert finish methods to back method in first spotlight when guide is not first', () => {
    const seenGuidesVersions = [];
    const guides = [
      {
        version: 'TMP-1',
        spotlights: [
          <GuideSpotlight
            target="any-target"
            content="any-content"
            key="spotlight-1"
            actions={[
              { onClick: 'close', text: 'Close' },
              { onClick: 'next', text: 'Next' },
            ]}
          />,
        ],
      },
      {
        version: 'TMP-2',
        spotlights: [
          <GuideSpotlight
            target="any-target"
            content="any-content"
            key="spotlight-1"
            actions={[
              { onClick: 'close', text: 'Close' },
              { onClick: 'next', text: 'Next' },
            ]}
          />,
          <GuideSpotlight
            target="any-target"
            content="any-content"
            key="spotlight-1"
            actions={[
              { onClick: 'close', text: 'Close' },
              { onClick: 'back', text: 'Back' },
            ]}
          />,
        ],
      },
    ];

    const expected = {
      versions: ['TMP-1', 'TMP-2'],
      spotlights: [
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'close', text: 'Close' },
            { onClick: 'next', text: 'Next' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'close', text: 'Close' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
      ],
    };

    const result = getAllSpotlightsFromUnseenGuides(seenGuidesVersions, guides);
    sortSpotlights(result.spotlights);
    sortSpotlights(expected.spotlights);

    expect(result).toEqual(expected);
  });

  it('should correct guides to one guide', () => {
    const guides = mapGuideSchemesToReactComponents([
      {
        version: 'TMP-1',
        spotlightsSchemes: [
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['skip', 'remindLater', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'close'],
          },
        ],
      },
      {
        version: 'TMP-3',
        spotlightsSchemes: [
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['close', 'skip', 'remindLater', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'close'],
          },
        ],
      },
      {
        version: 'TMP-2',
        spotlightsSchemes: [
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['close', 'skip', 'remindLater', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'close'],
          },
        ],
      },
    ]);

    const expected = {
      versions: ['TMP-1', 'TMP-3', 'TMP-2'],
      spotlights: [
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'finish', text: 'Skip' },
            { onClick: 'next', text: 'Next' },
            { onClick: 'remindLater', text: 'Remind later' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'back', text: 'Back' },
            { onClick: 'next', text: 'Next' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'back', text: 'Back' },
            { onClick: 'next', text: 'Next' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'finish', text: 'Close' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
      ],
    };

    const result = getAllSpotlightsFromUnseenGuides([], guides);

    sortSpotlights(expected.spotlights);
    sortSpotlights(result.spotlights);

    expect(result).toEqual(expected);
  });

  it('should correct guides to one guide when middle has one spotlight', () => {
    const guides = mapGuideSchemesToReactComponents([
      {
        version: 'TMP-1',
        spotlightsSchemes: [
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['skip', 'remindLater', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'close'],
          },
        ],
      },
      {
        version: 'TMP-3',
        spotlightsSchemes: [
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['close', 'skip', 'remindLater', 'next'],
          },
        ],
      },
      {
        version: 'TMP-2',
        spotlightsSchemes: [
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['close', 'skip', 'remindLater', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'next'],
          },
          {
            target: 'any-target',
            content: 'any-content',
            actions: ['back', 'close'],
          },
        ],
      },
    ]);

    const expected = {
      versions: ['TMP-1', 'TMP-3', 'TMP-2'],
      spotlights: [
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'finish', text: 'Skip' },
            { onClick: 'next', text: 'Next' },
            { onClick: 'remindLater', text: 'Remind later' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'back', text: 'Back' },
            { onClick: 'next', text: 'Next' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'back', text: 'Back' },
            { onClick: 'next', text: 'Next' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'next', text: 'Next' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
        <GuideSpotlight
          target="any-target"
          content="any-content"
          key="spotlight-1"
          actions={[
            { onClick: 'finish', text: 'Close' },
            { onClick: 'back', text: 'Back' },
          ]}
        />,
      ],
    };

    const result = getAllSpotlightsFromUnseenGuides([], guides);

    sortSpotlights(expected.spotlights);
    sortSpotlights(result.spotlights);

    expect(result).toEqual(expected);
  });

  it('should return data from react component props', () => {
    const props = {
      target: 'any-target',
      content: 'any-content',
      position: 'bottom-center',
      header: 'Header',
      width: 400,
    };

    const guides = mapGuideSchemesToReactComponents([
      {
        version: 'TMP-1',
        spotlightsSchemes: [
          { ...props, actions: ['skip', 'remindLater', 'next'] },
        ],
      },
    ]);

    const result = getDataFromSpotlightReactComponent(guides[0].spotlights[0]);

    expect(result).toEqual(props);
  });
});
