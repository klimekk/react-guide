import React from 'react';
import expect from 'expect';
import { mount } from 'enzyme';
import GuideManager from '../../../main/js/guide/manager/guide.manager';
import MockContainer from '../__mocks__/MockContainer';
import GuideRepositoryMock from '../__mocks__/guideRepositoryMock';
import { mapGuideSchemesToReactComponents } from '../../../main/js/guide/util/guide.util';
import GuideSpotlight from '../../../main/js/guide/components/guideSpotlight.component';

describe('Guide Manager Tests', () => {
  it('should initialize versions correct', () => {
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([
      {
        version: 'TMP-1',
        spotlightsSchemes: [],
      },
      {
        version: 'TMP-2',
        spotlightsSchemes: [],
      },
      {
        version: 'TMP-4',
        spotlightsSchemes: [],
      },
    ]);
    const seenVersions = ['TMP-1', 'TMP-2', 'TMP-3'];
    const MockRepository = new GuideRepositoryMock(seenVersions);

    const guideManager = new GuideManager(
      mockContainer,
      guides,
      MockRepository,
    );

    const expectedVersions = ['TMP-4'];

    // don't need to call any method because setup methods is called in constructor
    guideManager.setup(guides).then(() => {
      const resultVersions = guideManager.versions;
      expect(resultVersions).toEqual(expectedVersions);
      return resultVersions;
    });
  });

  it('should initialize spotlights correct', () => {
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([
      {
        version: 'TMP-5',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: ['next'],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
      {
        version: 'TMP-2',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
      {
        version: 'TMP-4',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
    ]);
    const seenVersions = ['TMP-1', 'TMP-2', 'TMP-3'];
    const MockRepository = new GuideRepositoryMock(seenVersions);

    const guideManager = new GuideManager(
      mockContainer,
      guides,
      MockRepository,
    );

    guideManager
      .setup(guides)
      .then(() => {
        const resultSpotlights = guideManager.spotlights;
        expect(resultSpotlights).toHaveLength(2);
        return resultSpotlights;
      })
      .catch(console.warn);
  });

  it('should return undefined as onClick function when function name is unrecognized', () => {
    const MockRepository = new GuideRepositoryMock();
    const mockContainer = mount(<MockContainer />);
    const guideManager = new GuideManager(mockContainer, [], MockRepository);

    const spotlights = [
      <GuideSpotlight
        key="spotlight-1"
        target="any"
        content="any-content"
        actions={[{ text: 'ANY', onClick: 'unrecognized-method' }]}/>,
    ];

    const expectedActionsProps = [
      {
        text: 'ANY',
        onClick: undefined,
      },
    ];

    const resultMappedSpotlights = guideManager.addMethodsToSpotlights(
      spotlights,
    );

    expect(resultMappedSpotlights[0].props.actions).toEqual(
      expectedActionsProps,
    );
  });

  it('should increment spotlight index when next method is called', () => {
    const MockRepository = new GuideRepositoryMock();
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([
      {
        version: 'TMP-5',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: ['next'],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
      {
        version: 'TMP-2',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
      {
        version: 'TMP-4',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
    ]);

    const guideManager = new GuideManager(mockContainer, [], MockRepository);

    const expectedSpotlightIndex = 1;

    guideManager
      .setup(guides)
      .then(() => guideManager.next())
      .then(() => {
        const {
          activeSpotlight: resultSpotlightIndex,
        } = guideManager.reactComponent.state();

        expect(resultSpotlightIndex).toEqual(expectedSpotlightIndex);
      });
  });

  it('should decrement spotlight index when back method is called', () => {
    const MockRepository = new GuideRepositoryMock();
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([]);

    const guideManager = new GuideManager(
      mockContainer,
      guides,
      MockRepository,
    );

    const expectedSpotlightIndex = 0;

    guideManager
      .setup(guides)
      .then(() =>
        guideManager.reactComponent.setState(state => ({
          ...state,
          activeSpotlight: 1,
        })),
      )
      .then(() => guideManager.back())
      .then(() => {
        const {
          activeSpotlight: resultSpotlightIndex,
        } = guideManager.reactComponent.state();

        expect(resultSpotlightIndex).toEqual(expectedSpotlightIndex);
      });
  });

  it('should set null spotlight index when finish method is called', () => {
    const MockRepository = new GuideRepositoryMock();
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([]);

    const guideManager = new GuideManager(
      mockContainer,
      guides,
      MockRepository,
    );

    const expectedSpotlightIndex = null;

    guideManager
      .setup(guides)
      .then(() => guideManager.finish())
      .then(() => {
        const {
          activeSpotlight: resultSpotlightIndex,
        } = guideManager.reactComponent.state();

        expect(resultSpotlightIndex).toEqual(expectedSpotlightIndex);
      });
  });

  it('should call saveVersionsOfSeenGuides method when finish method is called', () => {
    const MockRepository = new GuideRepositoryMock();
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([]);
    const finishMethodSpy = jest.spyOn(
      MockRepository,
      'saveVersionsOfSeenGuides',
    );

    const guideManager = new GuideManager(
      mockContainer,
      guides,
      MockRepository,
    );

    guideManager
      .setup(guides)
      .then(() => guideManager.finish())
      .then(() => expect(finishMethodSpy).toHaveBeenCalled());
  });

  it('should set null spotlight index when remind later method is called', () => {
    const MockRepository = new GuideRepositoryMock();
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([]);

    const guideManager = new GuideManager(
      mockContainer,
      guides,
      MockRepository,
    );

    const expectedSpotlightIndex = null;

    guideManager.initializeActiveSpotlightsState();
    const {
      activeSpotlight: resultSpotlightIndex,
    } = guideManager.reactComponent.state();

    expect(resultSpotlightIndex).toEqual(expectedSpotlightIndex);
  });

  it('should initialize state with null value untill manager is not set up', () => {
    const MockRepository = new GuideRepositoryMock();
    const mockContainer = mount(<MockContainer />);
    const guides = mapGuideSchemesToReactComponents([
      {
        version: 'TMP-5',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: ['next'],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
      {
        version: 'TMP-2',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
      {
        version: 'TMP-4',
        spotlightsSchemes: [
          {
            target: 'any-spot',
            actions: [],
            header: 'Spot header',
            position: 'bottom left',
            content: 'Spot content',
          },
        ],
      },
    ]);

    const guideManager = new GuideManager(
      mockContainer,
      guides,
      MockRepository,
    );

    const expectedSpotlightIndex = null;

    guideManager.initializeActiveSpotlightsState();
    const {
      activeSpotlight: resultSpotlightIndex,
    } = guideManager.reactComponent.state();

    expect(resultSpotlightIndex).toEqual(expectedSpotlightIndex);
  });
});
