import React from 'react';
import shortId from 'shortid';
import GuideSpotlight from '../components/guideSpotlight.component';

const ACTIONS = {
  close: {
    onClick: 'finish',
    text: 'Close',
  },
  next: {
    onClick: 'next',
    text: 'Next',
  },
  back: {
    onClick: 'back',
    text: 'Back',
  },
  skip: {
    onClick: 'finish',
    text: 'Skip',
  },
  remindLater: {
    onClick: 'remindLater',
    text: 'Remind later',
  },
  dismiss: {
    onClick: 'finish',
    text: 'Dismiss',
  },
  done: {
    onClick: 'finish',
    text: 'Done',
  },
};

function deleteLastSpotlight(spotlights) {
  return spotlights.slice(0, spotlights.length - 1);
}

function filterFinishMethods(actions) {
  return actions.filter(
    ({ onClick }) =>
      !['finish', 'close', 'remindLater'].some(e => e === onClick),
  );
}

function isNextMethodExist(actions) {
  return actions.some(({ onClick }) => onClick === 'next');
}

function isBackMethodExist(actions) {
  return actions.some(({ onClick }) => onClick === 'back');
}

function addNextMethod(actions) {
  const nextAction = ACTIONS.next;

  if (isNextMethodExist(actions)) {
    return actions;
  }

  return [...actions, nextAction];
}

function addBackMethod(actions) {
  const backAction = ACTIONS.back;

  if (isBackMethodExist(actions)) {
    return actions;
  }

  return [backAction, ...actions];
}

function convertLastSpotlightFinishToNextMethod(spotlights) {
  const firstSpotlight = spotlights[0];
  const isOneSpotlight = spotlights.length === 1;

  if (isOneSpotlight) {
    return {
      ...firstSpotlight,
      props: {
        ...firstSpotlight.props,
        actions: [...addNextMethod(firstSpotlight.props.actions)],
      },
    };
  }

  const lastSpotlight = spotlights[spotlights.length - 1];

  return {
    ...lastSpotlight,
    props: {
      ...lastSpotlight.props,
      actions: [
        ...addNextMethod(filterFinishMethods(lastSpotlight.props.actions)),
      ],
    },
  };
}

function convertFirstSpotlightFinishToBackMethod(spotlights) {
  const spotlight = spotlights[0];
  const isOneSpotlight = spotlights.length === 1;

  if (isOneSpotlight) {
    return {
      ...spotlight,
      props: {
        ...spotlight.props,
        actions: [...addBackMethod(spotlight.props.actions)],
      },
    };
  }
  return {
    ...spotlight,
    props: {
      ...spotlight.props,
      actions: [...addBackMethod(filterFinishMethods(spotlight.props.actions))],
    },
  };
}

function deleteFirstSpotlight(spotlights) {
  return spotlights.slice(1, spotlights.length);
}
export function isGuideWasNotSeen(seenGuidesVersions, guide) {
  return !seenGuidesVersions.some(v => v === guide.version);
}

function prepareFirstGuide(spotlights) {
  return [
    ...deleteLastSpotlight(spotlights),
    convertLastSpotlightFinishToNextMethod(spotlights),
  ];
}

function prepareLastGuide(spotlights) {
  return [
    convertFirstSpotlightFinishToBackMethod(spotlights),
    ...deleteFirstSpotlight(spotlights),
  ];
}

function prepareMiddleGuide(spotlights) {
  const spotlightsWithoutFirstAndLastElements = spotlights.slice(
    1,
    spotlights.length - 1,
  );

  if (spotlights.length === 1) {
    const firstSpotlightWithBackAndNextMethod = spotlights.map(spot => ({
      ...spot,
      props: {
        ...spot.props,
        actions: [
          ...filterFinishMethods([
            ...addBackMethod([...addNextMethod(spot.props.actions)]),
          ]),
        ],
      },
    }));

    return [firstSpotlightWithBackAndNextMethod[0]];
  }

  return [
    convertFirstSpotlightFinishToBackMethod(spotlights),
    ...spotlightsWithoutFirstAndLastElements,
    convertLastSpotlightFinishToNextMethod(spotlights),
  ];
}

export function getAllSpotlightsFromUnseenGuides(seenGuidesVersions, guides) {
  const mergedSpotlights = [];
  const versions = [];

  const unseendGuides = guides.filter(guide =>
    isGuideWasNotSeen(seenGuidesVersions, guide),
  );

  if (unseendGuides.length === 1) {
    return {
      spotlights: unseendGuides[0].spotlights,
      versions: [unseendGuides[0].version],
    };
  }

  unseendGuides.forEach((guide, index) => {
    let { spotlights } = guide;
    const { version } = guide;

    versions.push(version);

    const isFirstGuide = index === 0;
    const isLastGuide = index === unseendGuides.length - 1;

    if (isFirstGuide) {
      spotlights = prepareFirstGuide(spotlights);
    } else if (isLastGuide) {
      spotlights = prepareLastGuide(spotlights);
    } else {
      spotlights = prepareMiddleGuide(spotlights);
    }

    mergedSpotlights.push(...spotlights);
  });

  return {
    spotlights: mergedSpotlights,
    versions,
  };
}

export function getInitValue(spotlights) {
  return spotlights.length ? 0 : null;
}

function mapActionsNameToObjects(actions) {
  return actions.map(action => ACTIONS[action]);
}

function generateSpotlightKey() {
  return `spotlight-${shortId.generate()}`;
}

function mapSpotlightSchemeJsonToReactComponents(spotlightSchemes) {
  return spotlightSchemes.map(scheme => {
    const mappedScheme = {
      ...scheme,
      actions: mapActionsNameToObjects(scheme.actions),
    };

    return <GuideSpotlight key={generateSpotlightKey()} {...mappedScheme} />;
  });
}

export function mapGuideSchemesToReactComponents(guideSchemes) {
  return guideSchemes.map(guidScheme => {
    const { version, spotlightsSchemes } = guidScheme;

    return {
      version,
      spotlights: mapSpotlightSchemeJsonToReactComponents(spotlightsSchemes),
    };
  });
}

export function getDataFromSpotlightReactComponent(spotlight) {
  const { header, position, content, target, width } = spotlight.props;

  return {
    header,
    position,
    content,
    target,
    width,
  };
}
