import { mapGuideSchemesToReactComponents } from './util/guide.util';
import GuideFacade from './guide.facade';

export default class GuideFactory {
  static createGuideManagerWithGuideSchemes(
    reactComponent,
    guidesSchemes,
    guideRepository,
  ) {
    const guides = mapGuideSchemesToReactComponents(guidesSchemes);
    return new GuideFacade(reactComponent, guides, guideRepository);
  }

  static createGuideManager(reactComponents, guides, guideRepository) {
    return new GuideFacade(reactComponents, guides, guideRepository);
  }
}
